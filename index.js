/**
 * Bài 1
 * Đầu vào: lương nhân viên/ 1 ngày và số ngày làm.
 * Các bước xử lý: 
 * + Bước 1 : Tạo biến luongCoBan gán giá trị là 100.000
 * + Bước 2 : Tạo biến soNgayLam cho số ngày làm việc
 * + Bước 3 : Tạo biến tongLuong nhận giá trị là tongLuong = luongCoBan * soNgayLam.
 * + Bước 4 : In kết quả tongLuong theo biểu mẩu ra console.
 * Đầu ra: Tổng lương nhân viên nhận được.
 */
/**
 * Bài 2:
 * Đầu vào: 5 số thực bất kì
 * Các bước xử lí: 
 * + Bước 1: Khai báo 5 biến tương ứng cho 5 số thực
 * + Bước 2: Khai báo biến trungBinh để nhận giá trị là trungBinhCong = (Tổng 5 số)/5 
 * + Bước 3: In kết quả là giá trị biến trungBinhCong theo biểu mẫu ra console.
 * Đầu ra: Giá trị trung bình của 5 số thực
 */
/**
 * Bài 3:
 * Đầu vào: Giá trị 1USD = 23.500VND, số USD.
 * Các bước xử lí:
 * + Bước 1: Gán giá trị cho biến USD = 23.500
 * + Bước 2: Khai báo biến numUSD là số USD được nhập bất kì.
 * + Bước 3: Tạo biến VND chứa giá trị VND = (USD * numUSD)
 * + Bước 4: In giá trị của biến VND theo biểu mẫu ra console.
 * Đầu ra: Kết quả như biểu mẫu, xuất ra số tiền sau quy đổi VND.
 */
/**
 * Bài 4:
 * Đầu vào: Chiều dài, chiều rộng của hình chữ nhật
 * Các bước xử lí:
 * + Bước 1: Tạo biến chieuDai và gán giá trị cho chiều dài 
 * + Bước 2: Tạo biến chieuRong gán giá trị cho chiều rộng 
 * + Bước 3: Tạo biến chuVi nhận giá trị phép tính chuVi = (chieuDai + chieuRong) / 2
 * + Bước 4: Tạo biến dienTich nhận giá trị phép tính dienTich = (chieuDai * chieuRong);
 * + Bước 5: In giá trị chu vi (chuVi) và diện tích (dienTich) theo biểu mẫu ra console.
 * Đầu ra: Diện tích và Chu vi của hình chữ nhật.
 */
/**
  * Bài 5:
  * Đầu vào: Một số bất kì gồm 2 ký số 
  * Các bước xử lí:
  * + Bước 1: Tạo biến là num có giá trị một số gồm hai ký số
  * + Bước 2: Tạo biến là numDonVi để lấy số hàng đơn vị
  * + Bước 3: Tạo biến là numHangChuc để lấy số hàng chục
  * + Bước 4: Tạo 1 biến là tongHaiKySo để nhận giá trị phép tính  var tongHaiKySo = numDonVi + numHangChuc;
  * + Bước 5: In giá trị tongHaiKySo theo biểu mẫu ra console
  * Đầu ra: Tổng hai ký số của số đã nhập
  */
// EX1
function tinhLuong(){
    var soNgayLuong = document.getElementById("soNgayLam").value*1;
    var tienLuong =soNgayLuong*100000;
    document.getElementById("kqluong").innerHTML=`Tiền lương là: ${tienLuong} VND`;
}
//EX2
function trungbinh(){
    var so1=document.getElementById("so1").value*1;
    var so2=document.getElementById("so2").value*1;
    var so3=document.getElementById("so3").value*1;
    var so4=document.getElementById("so4").value*1;
    var so5=document.getElementById("so5").value*1;
    var trungbinhcong=(so2+so1+so3+so4+so5)/5;
    document.getElementById("kqtrungbinh").innerHTML=`Trung bình của 5 số là: ${trungbinhcong}`;
}
//EX3
function doitien(){
    var soUSD=document.getElementById("soUSD").value*1;
    tongtien=soUSD*23500;
    document.getElementById("kqquydoi").innerHTML=`Số tiền quy đổi thành : ${tongtien} VND`;
}
//EX4
function Tinh()
{
    var chieudai=document.getElementById("chieudai").value*1;
    var chieurong=document.getElementById("chieurong").value*1;
    var chuVi = (chieudai+chieurong)*2;
    var dienTich = chieudai*chieurong;
    document.getElementById("kq").innerHTML=`Chu vi là: <span class="text-danger"> ${chuVi} </span> và diện tích là: <span class="text-danger"> ${dienTich} </span>`;
}
//EX5
function tong2kyso()
{
    var so=document.getElementById("so").value*1;
    var donvi=so%10;
    var hangchuc=(so-donvi)/10;
    var tongkyso=donvi+hangchuc;
    document.getElementById("kqkyso").innerHTML=`Tổng của 2 ký số là: ${tongkyso}`;
}